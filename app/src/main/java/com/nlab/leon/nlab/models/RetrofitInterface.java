package com.nlab.leon.nlab.models;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RetrofitInterface {

    // for environments

    @GET("environment/")
    public Call<List<Environment>> getAllEnvironments();

    @GET("environment/{id}/")
    public Call<Environment> getEnvironment(@Path("id") Integer id);

    @PATCH("environment/{id}/")
    public Call<Environment> saveEnvironment(@Path("id") Integer id, @Body Environment env);

    @DELETE("environment/{id}/")
    public Call<Environment> removeEnvironment(@Path("id") Integer id);

    @GET("environment/{id}/build/")
    public Call<Environment> buildEnvironment(@Path("id") Integer id);

    @POST("environment/{id}/params/")
    public Call<EnvironmentParam> addEnvParam(@Path("id") Integer id, @Body EnvironmentParam env);

    @PATCH("environment/{id}/params/{param_id}/")
    public Call<EnvironmentParam> editEnvParam(@Path("id") Integer id,  @Path("param_id") Integer param_id, @Body EnvironmentParam env);

    @GET("environment/{id}/params/{param_id}/")
    public Call<EnvironmentParam> getEnvParam(@Path("id") Integer id, @Path("param_id") Integer param_id);

    @POST("environment/")
    public Call<Environment> createEnvironment(@Body Environment env);

    // for learnings

    @GET("learning/")
    public Call<List<Learning>> getAllLearnings();

    @GET("learning/{id}/")
    public Call<Learning> getLearning(@Path("id") Integer id);

    @PATCH("learning/{id}/")
    public Call<Learning> saveLearning(@Path("id") Integer id, @Body Learning lnr);

    @DELETE("learning/{id}/")
    public Call<Learning> removeLearning(@Path("id") Integer id);

    @GET("learning/{id}/get_population/")
    public Call<LearningParam> addLnrParam(@Path("id") Integer id, @Body LearningParam lnr);

    @GET("learning/{id}/get_state/")
    public Call<LearningParam> getLnrParam(@Path("id") Integer id, @Path("param_id") Integer param_id);

    @POST("environment/")
    public Call<Learning> createEnvironment(@Body Learning lnr);

    @POST("learning/{id}/pause/")
    public Call<Learning> createLearning(@Body Environment lnr);

}
