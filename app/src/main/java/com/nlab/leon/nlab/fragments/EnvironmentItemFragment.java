package com.nlab.leon.nlab.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nlab.leon.nlab.R;
import com.nlab.leon.nlab.app.App;
import com.nlab.leon.nlab.models.Environment;
import com.nlab.leon.nlab.models.EnvironmentParam;
import com.nlab.leon.nlab.models.RetrofitInterface;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// This fragment needs to present selected by user environment, where he can see it's
// parameters, change it, add new and delete current environment

public class EnvironmentItemFragment extends Fragment {

    public static String EXTRA_ITEM = "item";
    private String item;
    RetrofitInterface service = App.getRestClient().getRetrofitInstance();
    private boolean new_item;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = this.getArguments();

        if (bundle != null) {
            item = bundle.getString(EXTRA_ITEM);
        } else {
            new_item = true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.env_save_button:

                Objects.requireNonNull(getView()).findViewById(R.id.env_progress).setVisibility(View.VISIBLE);
                Objects.requireNonNull(getView()).findViewById(R.id.env_progress).bringToFront();

                Objects.requireNonNull(getActivity()).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                saveEnv(getView());

                return true;
            case R.id.env_build_button:
                buildEnv(getView());
                item.setEnabled(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void buildEnv(View view) {
        if (item != null) {
            Call<Environment> call = service.buildEnvironment(Integer.valueOf(item));
            call.enqueue(new Callback<Environment>() {
                @Override
                public void onResponse(@NonNull Call<Environment> call, @NonNull Response<Environment> response) {
                    if (response.body() != null) {
                        renderResponse(view, response.body());
                        setUpToolbar(response.body().getName());
                        Toast.makeText(getActivity(), "Build just started.", Toast.LENGTH_SHORT).show();
                    } else if (response.code() == 403) {
                        Toast.makeText(getActivity(), "Environment is building.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Environment> call, @NonNull Throwable t) {
                    Log.d("egegfrege", "error loading from API");
                }
            });
        }
    }

    private void saveEnv(View view) {

        final TextView env_id = getView().findViewById(R.id.env_id);
        String env_id_val = env_id.getText().toString();
        final TextView env_name = getView().findViewById(R.id.environmentName);
        String env_name_val = env_name.getText().toString();
        final TextView env_command = getView().findViewById(R.id.environmentCommand);
        String env_command_val = env_command.getText().toString();
        final TextView repository = getView().findViewById(R.id.repo);
        String repository_val = repository.getText().toString();
        final TextView env_url = getView().findViewById(R.id.registry);
        String env_url_val = env_url.getText().toString();

        Environment body = new Environment(env_url_val, env_name_val, repository_val, env_command_val);
        Call<Environment> call;
        if (item != null) {
            call = service.saveEnvironment(Integer.valueOf(item), body);
        } else if (new_item) {
            call = service.createEnvironment(body);
        } else {
            return;
        }

        call.enqueue(new Callback<Environment>() {
            @Override
            public void onResponse(@NonNull Call<Environment> call, @NonNull Response<Environment> response) {
                if (response.body() != null) {
                    renderResponse(view, response.body());
                    setUpToolbar(response.body().getName());
                }
                Activity activity = getActivity();
                Objects.requireNonNull(getView()).findViewById(R.id.env_progress).setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Saved", Toast.LENGTH_SHORT).show();
                Objects.requireNonNull(getActivity()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                if (activity != null) {
                    hideKeyboard(activity);
                    activity.onBackPressed();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Environment> call, @NonNull Throwable t) {
                Log.d("egegfrege", "error loading from API");
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu, menu);
        if (item != null) {
            MenuItem searchMenuItem = menu.findItem(R.id.env_build_button);
            searchMenuItem.setVisible(true);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_environment_item, container, false);
        if (item != null) {
            Button button = view.findViewById(R.id.remove_env);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> delete_environment());
        }

        Button add = view.findViewById(R.id.env_add_param);
        add.setOnClickListener(view1 -> {
            renderEnvParam(null);
        });

        return view;
    }

    private void addEnvParam(AlertDialog dialog, String key, String desc, String checkedParam, boolean isChecked, EnvironmentParam environmentParam) {
        EnvironmentParam body = new EnvironmentParam(key, desc, isChecked, checkedParam);

        Call<EnvironmentParam> call;
        if (item != null) {
            call = service.addEnvParam(Integer.valueOf(item), body);
        } else if (item != null && environmentParam != null) {
            call = service.editEnvParam(Integer.valueOf(item), environmentParam.getId(),body);
        } else {
            return;
        }

        call.enqueue(new Callback<EnvironmentParam>() {
            @Override
            public void onResponse(@NonNull Call<EnvironmentParam> call, @NonNull Response<EnvironmentParam> response) {
                if (response.body() != null) {
                    getAndRenderEnv();
                }
                Activity activity = getActivity();
                if (activity != null) {
                    hideKeyboard(activity);
                }
            }

            @Override
            public void onFailure(@NonNull Call<EnvironmentParam> call, @NonNull Throwable t) {
                Log.d("egegfrege", "error loading from API");
            }
        });
        Toast.makeText(getActivity(),
                "Done",
                Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    private void delete_environment() {
        Objects.requireNonNull(getView()).findViewById(R.id.env_progress).setVisibility(View.VISIBLE);

        Objects.requireNonNull(getActivity()).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        Activity activity = getActivity();

        if (activity != null && item != null) {
            Call<Environment> call = service.removeEnvironment(Integer.valueOf(item));
            call.enqueue(new Callback<Environment>() {
                @Override

                public void onResponse(@NonNull Call<Environment> call, @NonNull Response<Environment> response) {
                    if (response.body() != null || response.code() == 204) {
                        Objects.requireNonNull(getView()).findViewById(R.id.env_progress).setVisibility(View.GONE);
                        Objects.requireNonNull(getActivity()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT).show();
                        hideKeyboard(activity);
                        activity.onBackPressed();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Environment> call, @NonNull Throwable t) {
                    Log.d("egegfrege", "error loading from API");
                }
            });

        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        getAndRenderEnv();
    }

    private void getAndRenderEnv() {
        if (item != null) {
            Call<Environment> call = service.getEnvironment(Integer.valueOf(item));
            call.enqueue(new Callback<Environment>() {
                @Override
                public void onResponse(@NonNull Call<Environment> call, @NonNull Response<Environment> response) {
                    if (response.body() != null) {
                        renderResponse(Objects.requireNonNull(getView()), response.body());
                        setUpToolbar(response.body().getName());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Environment> call, @NonNull Throwable t) {
                    Log.d("egegfrege", "error loading from API");
                }
            });
        }
        if (new_item) {
            setUpToolbar("New environment");
        }
    }

    private void renderResponse(View view, Environment environment) {
        final EditText editText = view.findViewById(R.id.registry);
        editText.setText(environment.getRegistry_url());
        final EditText envName = view.findViewById(R.id.environmentName);
        envName.setText(environment.getName());
        final EditText env_command = view.findViewById(R.id.environmentCommand);
        env_command.setText(environment.getCommand());
        final EditText repo = view.findViewById(R.id.repo);
        repo.setText(environment.getRepo_url());
        final TextView env_id = view.findViewById(R.id.env_id);
        env_id.setText(Integer.toString(environment.getId()));
        if (environment.getMessage() != null) {
            final TextView env_build_message = view.findViewById(R.id.env_build_message);
            env_build_message.setText(environment.getMessage());
            env_build_message.setVisibility(View.VISIBLE);
        }

        final TextView envState = view.findViewById(R.id.envState);
        envState.setText(environment.getStateRepr());
        envState.setTextColor(environment.getStateColor());

        LinearLayout linearLayout = view.findViewById(R.id.linearLayout);
        linearLayout.removeAllViewsInLayout();

        for (int x = 0; x < environment.getParams().size(); x = x + 1) {

            TextView envname = new TextView(getActivity());
            LinearLayout.LayoutParams a = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            a.setMargins(16,16,16,16);
            envname.setLayoutParams(a);
            envname.setText(environment.getParams().get(x).getKey());
            envname.setTextSize(20);
            envname.setBackgroundColor(Color.GRAY);
            int finalX = x;
            envname.setOnClickListener(v -> {
                {
                    Integer id = environment.getParams().get(finalX).getId();
                    getAndRenderEnvParam(id);
                }
            });
            linearLayout.addView(envname);
        }
    }

    private void getAndRenderEnvParam(Integer id) {
        Call<EnvironmentParam> call = service.getEnvParam(Integer.valueOf(item), id);
        call.enqueue(new Callback<EnvironmentParam>() {
            @Override
            public void onResponse(@NonNull Call<EnvironmentParam> call, @NonNull Response<EnvironmentParam> response) {
                renderEnvParam(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<EnvironmentParam> call, @NonNull Throwable t) {
                Log.d("egegfrege", "error loading from API");
            }
        });

    }

    private void renderEnvParam(EnvironmentParam environmentParam) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View mView = getLayoutInflater().inflate(R.layout.dialog_add_env_param, null);

        final EditText envParamKey = mView.findViewById(R.id.envParamKey);
        final EditText envParamDesc = mView.findViewById(R.id.envParamDesc);
        Button btnAddEnvParam = mView.findViewById(R.id.btnAddEnvParam);

        RadioGroup radioButtonGroup = mView.findViewById(R.id.paramsEnvRadio);

        if (environmentParam != null) {
            envParamKey.setText(environmentParam.getKey());
            envParamDesc.setText(environmentParam.getDescription());
            CheckBox check = mView.findViewById(R.id.paramRequired);
            check.setChecked(environmentParam.isRequired());
        }

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnAddEnvParam.setOnClickListener(view2 -> {
            String key = envParamKey.getText().toString();
            String desc = envParamDesc.getText().toString();

            int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
            View radioButton = radioButtonGroup.findViewById(radioButtonID);
            int idx = radioButtonGroup.indexOfChild(radioButton);
            RadioButton r = (RadioButton) radioButtonGroup.getChildAt(idx);
            String checkedParam = r.getText().toString();

            boolean isChecked = ((CheckBox) mView.findViewById(R.id.paramRequired)).isChecked();

            if (key.isEmpty()) {
                Toast.makeText(getActivity(),
                        "Key shouldn't be empty",
                        Toast.LENGTH_SHORT).show();
            } else {
                addEnvParam(dialog, key, desc, checkedParam, isChecked, environmentParam);
            }
        });
    }


    private void setUpToolbar(String name) {
        Activity activity = getActivity();
        if (activity != null) {
            Toolbar toolbar = activity.findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
            toolbar.setTitle(name);
        }
    }
}