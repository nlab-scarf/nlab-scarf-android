package com.nlab.leon.nlab.models;

import com.google.gson.annotations.SerializedName;

// Interface for requests realization for learnings
// API documentation is on the server

public class Learning {

    @SerializedName("id")
    private Integer id;

    @SerializedName("state")
    private Integer state;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("pub_date")
    private String pub_date;

    @SerializedName("name")
    private String name;

    @SerializedName("environment")
    private String environment;

    @SerializedName("nlab")
    private String nlab;

    @SerializedName("network")
    private String network;

    @SerializedName("deploy_address")
    private String deploy_address;

    @SerializedName("rounds")
    private String rounds;

    @SerializedName("pop_size")
    private String pop_size;

    public Learning(Integer id, Integer state, String created_at, String pub_date, String name,
                    String environment, String nlab, String network, String deploy_address, String round, String pop_size){
        this.id = id;
        this.state = state;
        this.created_at = created_at;
        this.pub_date = pub_date;
        this.name = name;
        this.environment = environment;
        this.nlab = nlab;
        this.network = network;
        this.deploy_address = deploy_address;
        this.rounds = round;
        this.pop_size = pop_size;
    }


    public String getDeploy_address() {
        return deploy_address;
    }

    public void setDeploy_address(String deploy_address) {
        this.deploy_address = deploy_address;
    }

    public String getRounds() {
        return rounds;
    }

    public void setRounds(String rounds) {
        this.rounds = rounds;
    }

    public String getPop_size() {
        return pop_size;
    }

    public void setPop_size(String pop_size) {
        this.pop_size = pop_size;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getNlab() {
        return nlab;
    }

    public void setNlab(String nlab) {
        this.nlab = nlab;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPub_date() {
        return pub_date;
    }

    public void setPub_date(String pub_date) {
        this.pub_date = pub_date;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
