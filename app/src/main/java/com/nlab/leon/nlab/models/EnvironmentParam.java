package com.nlab.leon.nlab.models;

import com.google.gson.annotations.SerializedName;

// Serialized data for representation in the application
// This list is for environments

public class EnvironmentParam {
    @SerializedName("id")
    private Integer id;
    @SerializedName("key")
    private String key;
    @SerializedName("description")
    private String description;
    @SerializedName("required")
    private boolean required;
    @SerializedName("type")
    private Integer type;
    @SerializedName("environment")
    private Integer environment;

    public EnvironmentParam(Integer id, String key, String description, boolean required, Integer type, Integer environment) {
        this.id = id;
        this.key = key;
        this.description = description;
        this.required = required;
        this.type = type;
        this.environment = environment;
    }

    public EnvironmentParam(String key, String description, boolean required, String type) {
        this.key = key;
        this.description = description;
        this.required = required;
        this.type = getTypeByString(type);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getEnvironment() {
        return environment;
    }

    public void setEnvironment(Integer environment) {
        this.environment = environment;
    }

    public Integer getTypeByString(String str){
        switch (str) {
            case "Arg":
                return 2;
            case "Flag":
                return 1;
            case "Param":
                return 0;
            default:
                return 1;
        }
    }
}
