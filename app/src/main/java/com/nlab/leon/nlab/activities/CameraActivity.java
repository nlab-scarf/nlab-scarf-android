package com.nlab.leon.nlab.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.nlab.leon.nlab.R;

import java.io.File;

public class CameraActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 0;
    private ImageView imageView_camera;
    private Uri outputFileUri;
    // camera and saving photo

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        imageView_camera = findViewById(R.id.imageView_camera);
    }

    public void onClick_save(View view) {
        //getThumbnailPicture();
        saveFullImage();
    }

    public void onClick(View v) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            // Извлечение фотографии
            Bitmap thumbnailBitmap = (Bitmap) data.getExtras().get("data");
            imageView_camera.setImageBitmap(thumbnailBitmap); // Размещаем фото в ImageView
        }
    }

    private void saveFullImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(),
                "test.jpg");
        outputFileUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        int TAKE_PICTURE_REQUEST = 1;
        startActivityForResult(intent, TAKE_PICTURE_REQUEST);
    }
}
