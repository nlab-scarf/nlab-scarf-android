package com.nlab.leon.nlab.common;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import static android.content.Context.MODE_PRIVATE;

public final class Preferences {
    private static String PREFS_NLAB = "prefs_nlab";
    private static String PREF_TOKEN = "token";

    private static Preferences prefsInstance;
    private static SharedPreferences sharedPreferences;

    public static void createInstance(@NonNull final Activity activity) {
        prefsInstance = new Preferences();
        sharedPreferences = activity.getSharedPreferences(PREFS_NLAB, MODE_PRIVATE);
    }

    public static Preferences getInstance() {
        if (prefsInstance == null) {
            prefsInstance = new Preferences();
        }

        return prefsInstance;
    }

    public final String getToken() {
        return sharedPreferences.getString(PREF_TOKEN, null);
    }

    public final void setToken(@NonNull final String token) {
        sharedPreferences.edit().putString(PREF_TOKEN, token).apply();
    }
}
