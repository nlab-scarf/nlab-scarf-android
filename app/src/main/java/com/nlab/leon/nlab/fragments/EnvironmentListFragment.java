package com.nlab.leon.nlab.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nlab.leon.nlab.R;
import com.nlab.leon.nlab.adapters.EnvironmentListAdapter;
import com.nlab.leon.nlab.app.App;
import com.nlab.leon.nlab.models.Environment;
import com.nlab.leon.nlab.models.RetrofitInterface;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nlab.leon.nlab.fragments.EnvironmentItemFragment.EXTRA_ITEM;

// This fragment needs to present list of all environments that are currently on server
// User can choose one to operate with it

public class EnvironmentListFragment extends Fragment implements EnvironmentListAdapter.OnItemClickListener {
    private SwipeRefreshLayout swipeLayout;
    RetrofitInterface service = App.getRestClient().getRetrofitInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_environment_list, container, false);
        FloatingActionButton fab = view.findViewById(R.id.addFab);
        fab.setOnClickListener(view1 -> {
            final EnvironmentItemFragment envFragment = new EnvironmentItemFragment();
            Activity activity = getActivity();
            if (activity != null) {
                ((FragmentActivity) activity).getSupportFragmentManager().beginTransaction().replace(R.id.main_container,
                        envFragment).addToBackStack(null).commit();

            }
        });
        swipeLayout = view.findViewById(R.id.swipeContainer);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        SwipeRefreshLayout swipeContainer = Objects.requireNonNull(getView()).findViewById(R.id.swipeContainer);

        EnvironmentListFragment envFr = this;
        fetch(recyclerView, envFr);

        swipeContainer.setOnRefreshListener(() -> fetch(recyclerView, envFr));

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        setUpToolbar();
    }

    private void fetch(RecyclerView recyclerView, EnvironmentListFragment envFr){
        Call<List<Environment>> call = service.getAllEnvironments();
        call.enqueue(new Callback<List<Environment>>() {
            @Override
            public void onResponse(@NonNull Call<List<Environment>> call, @NonNull Response<List<Environment>> response) {
                EnvironmentListAdapter adp = new EnvironmentListAdapter(envFr);
                recyclerView.setAdapter(adp);

                adp.clear();
                adp.addAll(response.body());
                swipeLayout.setRefreshing(false);
            }
            @Override
            public void onFailure(@NonNull Call<List<Environment>> call, @NonNull Throwable t) {
                Log.d("egegfrege", "error loading from API");
            }
        });
    }

    @Override
    public void onItemClick(@NonNull String item) {
        final EnvironmentItemFragment envFragment = new EnvironmentItemFragment();
        final Bundle bundle = new Bundle();
        bundle.putString(EXTRA_ITEM, item);
        envFragment.setArguments(bundle);

        Activity activity = getActivity();
        if (activity != null) {
            ((FragmentActivity) activity).getSupportFragmentManager().beginTransaction().replace(R.id.main_container,
                    envFragment).addToBackStack(null).commit();

        }
    }

    private void setUpToolbar() {
        Activity activity = getActivity();
        if (activity != null) {
            Toolbar toolbar = activity.findViewById(R.id.toolbar);
            toolbar.setTitle(R.string.nav_environments);
            toolbar.setNavigationIcon(R.drawable.ic_menu);
        }
    }
}

