package com.nlab.leon.nlab.models;

import android.graphics.Color;

import com.google.gson.annotations.SerializedName;

import java.util.List;

// Interface for requests realization for environments
// API documentation is on the server
public class Environment {

    @SerializedName("id")
    private Integer id;

    @SerializedName("state")
    private Integer state;

    @SerializedName("registry_url")
    private String registry_url;
    @SerializedName("name")
    private String name;
    @SerializedName("repo_url")
    private String repo_url;

    @SerializedName("command")
    private String command;

    @SerializedName("message")
    private String message;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("params")
    private List<EnvironmentParam> params;// = new ArrayList<EnvironmentParam>();

    public List<EnvironmentParam> getParams() {
        return params;
    }

    public void setParams(List<EnvironmentParam> params) {
        this.params = params;
    }

    public Environment(Integer id, Integer state, String registry_url, String name, String repo_url,
                       String command, String message, String created_at, List<EnvironmentParam> params) {
        this.id = id;
        this.registry_url = registry_url;
        this.name = name;
        this.repo_url = repo_url;
        this.command = command;
        this.message = message;
        this.created_at = created_at;
        this.state = state;
        this.params = params;
    }

    public Environment(String registry_url, String name, String repo_url, String command) {
        this.registry_url = registry_url;
        this.name = name;
        this.repo_url = repo_url;
        this.command = command;
    }

    public Integer getId() {
        return id;
    }


    public String getRegistry_url() {
        return registry_url;
    }

    public void setRegistry_url(String registry_url) {
        this.registry_url = registry_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRepo_url() {
        return repo_url;
    }

    public void setRepo_url(String repo_url) {
        this.repo_url = repo_url;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getCreated_at() {
        return created_at;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStateRepr() {
        switch (this.state) {
            case 0:
                return "Created";
            case 1:
                return "Building";
            case 2:
                return "Ready";
            case 3:
                return "Error";
            default:
                return "No data";

        }
    }

    public Integer getStateColor() {
        switch (this.state) {
            case 0:
                return Color.GRAY;
            case 1:
                return Color.YELLOW;
            case 2:
                return Color.GREEN;
            case 3:
                return Color.RED;
            default:
                return Color.BLACK;
        }
    }
}

