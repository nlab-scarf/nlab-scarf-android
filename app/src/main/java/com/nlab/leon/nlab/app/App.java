package com.nlab.leon.nlab.app;

import android.app.Application;

import com.nlab.leon.nlab.models.RetrofitClientInstance;


public class App extends Application {
    private static RetrofitClientInstance restClient;

    @Override
    public void onCreate() {
        super.onCreate();
        restClient = new RetrofitClientInstance();
    }

    public static RetrofitClientInstance getRestClient() {
        return restClient;
    }
}