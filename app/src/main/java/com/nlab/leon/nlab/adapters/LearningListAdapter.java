package com.nlab.leon.nlab.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nlab.leon.nlab.R;
import com.nlab.leon.nlab.models.Learning;

import java.util.List;

// Adapter for list of all learnings
// This needs to represent all existing learnings on the server at user interface

public class LearningListAdapter extends RecyclerView.Adapter<LearningListAdapter.LearningViewHolder> {

    private List<Learning> items;
    private OnItemClickListener listener;

    public void clear() {
        if (items != null){
            items.clear();
        }
        notifyDataSetChanged();
    }

    public void addAll(List<Learning> list) {
        this.items = list;
        notifyDataSetChanged();
    }
    public interface OnItemClickListener {
        void onItemClick(@NonNull final String item);
    }


    public LearningListAdapter(@NonNull final OnItemClickListener onItemClickListener) {
        this.listener = onItemClickListener;
    }

    @NonNull
    @Override
    public LearningViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_learning, parent, false);
        return new LearningViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LearningViewHolder learningViewHolder, int i) {
        learningViewHolder.bind(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class LearningViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView nlab;
        private TextView rounds;
        private TextView pop_size;
        private TextView id;
        private TextView environment;
        private ImageView state;

        LearningViewHolder(@NonNull View view) {
            super(view);
            name = view.findViewById(R.id.lnr_list_title);
            nlab = view.findViewById(R.id.lnr_list_nlab);
            rounds = view.findViewById(R.id.lnr_list_rounds);
            pop_size = view.findViewById(R.id.lnr_list_pop_size);
            id = view.findViewById(R.id.lnr_list_id);
            environment = view.findViewById(R.id.lnr_list_environment);

            view.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(id.getText().toString());
                }
            });
        }

        void bind(@NonNull final Learning lnr) {

            name.setText(lnr.getName());
            nlab.setText(lnr.getNlab());
            rounds.setText(lnr.getRounds());
            pop_size.setText(lnr.getPop_size());
            id.setText(String.valueOf(lnr.getId()));
            environment.setText(lnr.getEnvironment());
            Integer lnr_state = lnr.getState();

        }
    }
}
