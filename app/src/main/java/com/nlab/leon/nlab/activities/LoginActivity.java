package com.nlab.leon.nlab.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.nlab.leon.nlab.R;
import com.nlab.leon.nlab.common.Preferences;

// Start activity of an application, there should be check of user logging status
// If the user already logged in -> To main Activity

public final class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Button btnLogin = findViewById(R.id.button_login);
        btnLogin.setOnClickListener(v -> {
            Preferences.getInstance().setToken("somewonderfultoken");
            startActivity(new Intent(this, MainActivity.class));
            finish();
        });

    }
}
