package com.nlab.leon.nlab.models;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitClientInstance {
    private RetrofitInterface retrofitInterface;
    private static Retrofit retrofit;
    private static final String BASE_URL = "http://192.241.153.110:8000/";


    public RetrofitClientInstance() {

        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        retrofitInterface = retrofit.create(RetrofitInterface.class);
    }

    public RetrofitInterface getRetrofitInstance() {return retrofitInterface;}
}

