package com.nlab.leon.nlab.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nlab.leon.nlab.R;
import com.nlab.leon.nlab.common.Preferences;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Preferences.createInstance(this);
        startActivity(new Intent(getApplicationContext(), Preferences.getInstance().getToken() != null ? MainActivity.class : LoginActivity.class));

        finish();
    }
}