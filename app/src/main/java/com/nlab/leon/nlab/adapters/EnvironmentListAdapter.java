package com.nlab.leon.nlab.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nlab.leon.nlab.R;
import com.nlab.leon.nlab.models.Environment;

import java.util.List;

// Adapter for list of all environments
// This needs to represent all existing environments on the server at user interface

public class EnvironmentListAdapter extends RecyclerView.Adapter<EnvironmentListAdapter.EnvironmentViewHolder> {

    private List<Environment> items;
    private OnItemClickListener listener;

    public void clear() {
        if (items != null){
            items.clear();
        }
        notifyDataSetChanged();
    }

    public void addAll(List<Environment> list) {
        this.items = list;
        notifyDataSetChanged();
    }
    public interface OnItemClickListener {
        void onItemClick(@NonNull final String item);
    }


    public EnvironmentListAdapter(@NonNull final OnItemClickListener onItemClickListener) {
        this.listener = onItemClickListener;
    }

    @NonNull
    @Override
    public EnvironmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_environment, parent, false);
        return new EnvironmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EnvironmentViewHolder environmentViewHolder, int i) {
        environmentViewHolder.bind(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class EnvironmentViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView repo;
        private TextView id;
        private ImageView state;

        EnvironmentViewHolder(@NonNull View view) {
            super(view);
            name = view.findViewById(R.id.env_list_title);
            id = view.findViewById(R.id.env_list_id);
            repo = view.findViewById(R.id.env_list_registry);
            state = view.findViewById(R.id.env_list_state);
            view.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(id.getText().toString());
                }
            });
        }

        void bind(@NonNull final Environment env) {

            name.setText(env.getName());
            id.setText(String.valueOf(env.getId()));
            repo.setText(env.getRegistry_url());
            Integer env_state = env.getState();
            Integer state_image;
            switch (env_state){
                case 0:
                    state_image = R.drawable.ic_env_created;
                    break;
                case 1:
                    state_image = R.drawable.ic_building;
                    break;
                case 2:
                    state_image = R.drawable.ic_ok_env;
                    break;
                case 3:
                    state_image = R.drawable.ic_error_environment;
                    break;
                default:
                    state_image = R.drawable.ic_env_created;

            }
            state.setImageResource(state_image);
        }
    }
}
