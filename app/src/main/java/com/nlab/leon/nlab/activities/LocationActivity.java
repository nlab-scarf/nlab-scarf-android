package com.nlab.leon.nlab.activities;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.location.Location;
import android.location.LocationManager;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import android.location.LocationManager;

import com.nlab.leon.nlab.R;

public class LocationActivity extends AppCompatActivity {

    private TextView dolgota;
    private TextView shirota;
    LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        LocationManager lm =
                (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        dolgota = findViewById(R.id.dolgota);
        shirota = findViewById(R.id.shirota);

    }

    public void onLocationChanged(Location location)
    {
        if (location != null)
        {

            dolgota.setText("altitude " +location.getLatitude());
            shirota.setText("longitude " +location.getLongitude()); // "Долгота="+location.getLongitude();
        }
    }

    @Override
    protected void onResume() {
        setTitle("location service");
        super.onResume();
    }

    private boolean isGPSEnabled() {
        LocationManager cm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return cm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

}
